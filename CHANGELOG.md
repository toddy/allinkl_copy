# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.0.3] – 2022-01-04

### Changed
* Use a valid SPDX license identifier in composer.json, otherwise
  packagist.org will reject the package


## [1.0.2] – 2022-01-04

### Added
* Add note about repository move to codeberg.org


## [1.0.1] – 2015-11-24

### Added
* Add minimal tested version of roundcube to composer.json

### Removed
* Remove deprecated methods for roundcube 1.2


## [1.0.0] - 2015-07-24

* Initial release


[1.0.3]: https://codeberg.org/toddy/allinkl_copy/compare/v1.0.2...v1.0.3
[1.0.2]: https://codeberg.org/toddy/allinkl_copy/compare/v1.0.1...v1.0.2
[1.0.1]: https://codeberg.org/toddy/allinkl_copy/compare/v1.0.0...v1.0.1
[1.0.0]: https://codeberg.org/toddy/allinkl_copy/src/tag/v1.0.0
